export type TiposIdentificacion = Array<{
  id: number;
  nombre: string;
}>;
