export interface EmpleadoFiltro {
  primer_nombre: string;
  otros_nombre: string;
  primer_apellido: string;
  segundo_apellido: string;
  tipo_identificacion: string;
  identificacion: string;
  pais: string;
  correo_electronico: string;
  estado: string;
}
