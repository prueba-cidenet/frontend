import { Empleado } from './Empleado';

interface Link {
  url?: string;
  label: string;
  active: boolean;
}

export interface PaginateEmpleados {
  current_page: number;
  data: Empleado[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Link[];
  next_page_url?: string;
  path: string;
  per_page: number;
  prev_page_url?: string;
  to: number;
  total: number;
}
