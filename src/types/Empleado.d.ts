export interface Empleado {
  id: number;
  primer_nombre: string;
  otros_nombre: string;
  primer_apellido: string;
  segundo_apellido: string;
  pais: string;
  tipo_identificacion: string;
  identificacion: string;
  correo_electronico: string;
  fecha_ingreso: string;
  area_id: number;
  estado: string;
  fecha_registro: string;
  pais_nombre: string;
  tipo_identificacion_nombre: string;
  area: { id: number; nombre: string };
}

export interface EmpleadoForm
  extends Omit<
    Empleado,
    | 'id'
    | 'area'
    | 'estado'
    | 'pais_nombre'
    | 'tipo_identificacion_nombre'
    | 'correo_electronico'
  > {
  fecha_registro: Date;
  fecha_ingreso: Date;
}

