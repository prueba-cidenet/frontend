import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Layout } from '@/components/Layout';
import { ListadoEmpleados } from '@/components/ListadoEmpleados';
import { AgregarEmpleado } from '@/components/AgregarEmpleado';
import { EditarEmpleado } from '@/components/EditarEmpleado';

export const App = () => (
  <BrowserRouter>
    <Layout>
      <Routes>
        <Route path="/" element={<ListadoEmpleados />} />
        <Route path="/add" element={<AgregarEmpleado />} />
        <Route path="/:id" element={<EditarEmpleado />} />
        {/* <Route path="*" element={<NotFound />} /> */}
      </Routes>
    </Layout>
  </BrowserRouter>
);
