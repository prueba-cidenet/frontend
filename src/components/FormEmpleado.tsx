import { useEffect, useState, ChangeEvent } from 'react';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import DateTimePicker from 'react-datetime-picker';
import { getTiposIdentificacion } from '@/services/TiposIdentificacionService';
import { getAreas } from '@/services/AreaService';
import { getPaises } from '@/services/PaisService';
import type { EmpleadoForm } from '@/types/Empleado';
import type { TiposIdentificacion } from '@/types/TiposIdentificacion';
import type { Areas } from '@/types/Areas';
import type { Paises } from '@/types/Paises';

const Recomendacion = (
  <Form.Text className="text-muted">Mayusculas sin acentos ni Ñ.</Form.Text>
);

type Props = {
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  empleado: EmpleadoForm;
  onChangeForm: (empleadoForm: EmpleadoForm) => void;
  errores: string[];
  isCreate: boolean;
};

export const FormEmpleado = ({
  onSubmit,
  empleado,
  onChangeForm,
  errores,
  isCreate,
}: Props) => {
  const [tiposIdentificacion, setTiposIdentificacion] =
    useState<TiposIdentificacion>([]);
  const [areas, setAreas] = useState<Areas>([]);
  const [paises, setPaises] = useState<Paises>([]);

  useEffect(() => {
    getTiposIdentificacion().then(response =>
      setTiposIdentificacion(response.data)
    );
    getAreas().then(response => setAreas(response.data));
    getPaises().then(response => setPaises(response.data));
  }, []);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) =>
    onChangeForm({ ...empleado, [e.target.name]: e.target.value });

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) =>
    onChangeForm({ ...empleado, [e.target.name]: e.target.value });

  return (
    <Form onSubmit={onSubmit}>
      {errores.length > 0 && (
        <Alert variant="danger">
          {errores.map(error => (
            <p className="mb-0" key={error}>
              {error}
            </p>
          ))}
        </Alert>
      )}

      <Row>
        <Col xs={6}>
          <Form.Group className="mb-3" controlId="primer_nombre">
            <Form.Label>Primer Nombre</Form.Label>
            <Form.Control
              value={empleado.primer_nombre}
              onChange={handleChange}
              name="primer_nombre"
              type="text"
            />
            {Recomendacion}
          </Form.Group>
          <Form.Group className="mb-3" controlId="primer_apellido">
            <Form.Label>Primer Apellido</Form.Label>
            <Form.Control
              value={empleado.primer_apellido}
              onChange={handleChange}
              name="primer_apellido"
              type="text"
            />
            {Recomendacion}
          </Form.Group>
          <Form.Group className="mb-3" controlId="tipo_identificacion">
            <Form.Label>Tipo de identificacion</Form.Label>
            <Form.Select
              value={empleado.tipo_identificacion}
              onChange={handleChangeSelect}
              name="tipo_identificacion"
            >
              <option>Seleccione una opción</option>
              {tiposIdentificacion.map(tipo => (
                <option value={tipo.id} key={tipo.id}>
                  {tipo.nombre}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
          <Form.Group className="mb-3" controlId="pais">
            <Form.Label>País</Form.Label>
            <Form.Select
              value={empleado.pais}
              onChange={handleChangeSelect}
              name="pais"
            >
              <option>Seleccione una opción</option>
              {paises.map(tipo => (
                <option value={tipo.id} key={tipo.id}>
                  {tipo.nombre}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
          <Form.Group className="mb-3" controlId="area_id">
            <Form.Label>Area</Form.Label>
            <Form.Select
              value={empleado.area_id}
              onChange={handleChangeSelect}
              name="area_id"
            >
              <option>Seleccione una opción</option>
              {areas.map(tipo => (
                <option value={tipo.id} key={tipo.id}>
                  {tipo.nombre}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
        </Col>
        <Col xs={6}>
          <Form.Group className="mb-3" controlId="otros_nombre">
            <Form.Label>Otros Nombre</Form.Label>
            <Form.Control
              value={empleado.otros_nombre}
              onChange={handleChange}
              name="otros_nombre"
              type="text"
            />
            {Recomendacion}
          </Form.Group>
          <Form.Group className="mb-3" controlId="segundo_apellido">
            <Form.Label>Segundo Apellido</Form.Label>
            <Form.Control
              value={empleado.segundo_apellido}
              onChange={handleChange}
              name="segundo_apellido"
              type="text"
            />
            {Recomendacion}
          </Form.Group>
          <Form.Group className="mb-3" controlId="identificacion">
            <Form.Label>Identificacion</Form.Label>
            <Form.Control
              value={empleado.identificacion}
              onChange={handleChange}
              name="identificacion"
              type="text"
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="fecha_ingreso">
            <Form.Label>Fecha ingreso</Form.Label>
            <DateTimePicker
              value={empleado.fecha_ingreso}
              onChange={e => onChangeForm({ ...empleado, fecha_ingreso: e })}
              format="y-MM-dd"
              className="form-control"
            />
          </Form.Group>
          {isCreate && (
            <Form.Group className="mb-3" controlId="fecha_registro">
              <Form.Label>Fecha registro</Form.Label>
              <DateTimePicker
                value={empleado.fecha_registro!}
                onChange={e => onChangeForm({ ...empleado, fecha_registro: e })}
                format="y-MM-dd h:mm"
                className="form-control"
              />
            </Form.Group>
          )}
        </Col>
      </Row>

      <Button variant="success" type="submit" className="mb-3">
        {isCreate ? 'Crear' : 'Editar'}
      </Button>
    </Form>
  );
};
