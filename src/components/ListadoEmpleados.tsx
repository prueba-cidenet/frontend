import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  MdOutlineAddCircle,
  MdFilterAlt,
  MdSkipPrevious,
  MdSkipNext,
  MdOutlineDelete,
  MdOutlineModeEditOutline,
} from 'react-icons/md';
import { deleteEmpleado, getEmpleados } from '@/services/EmpleadoService';
import {
  Table,
  ButtonGroup,
  Button,
  Badge,
  ButtonToolbar,
} from 'react-bootstrap';

import type { PaginateEmpleados } from '@/types/PaginateEmpleados';
import type { EmpleadoFiltro } from '@/types/EmpleadoFiltro';

import { FiltrosEmpleado } from './FiltrosEmpleado';

const INITIAL_PAGINATE = {
  current_page: 0,
  data: [],
  first_page_url: '',
  from: 0,
  last_page: 0,
  last_page_url: '',
  links: [],
  next_page_url: '',
  path: '',
  per_page: 0,
  prev_page_url: '',
  to: 0,
  total: 0,
};

const ICONS_PAGINATE: Record<string, any> = {
  '&laquo; Anterior': <MdSkipPrevious />,
  'Siguiente &raquo;': <MdSkipNext />,
};

export const ListadoEmpleados = () => {
  const [paginate, setPaginate] = useState<PaginateEmpleados>(INITIAL_PAGINATE);
  const [showFilters, setShowFilters] = useState(false);
  const [filtros, setFiltros] = useState<EmpleadoFiltro>({
    primer_nombre: '',
    otros_nombre: '',
    primer_apellido: '',
    segundo_apellido: '',
    tipo_identificacion: '',
    identificacion: '',
    pais: '',
    correo_electronico: '',
    estado: '',
  });

  const handleClose = () => setShowFilters(false);

  const onChangeFiltros = (nuevoFiltro: EmpleadoFiltro) => {
    setFiltros(nuevoFiltro);
  };

  const getData = (url = '/empleados') => {
    getEmpleados(url, filtros).then(response => {
      setPaginate(response.data);
    });
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = (id: number) => {
    if (window.confirm('¿Esta seguro que desea eliminar?')) {
      deleteEmpleado(id).then(() => {
        toast.success('Empleado eliminado con exito!');
        getData();
      });
    }
  };

  return (
    <>
      <div className="d-flex justify-content-between pb-3">
        <h1 className="h2">Listado de Empleados</h1>

        <ButtonGroup>
          <Button
            onClick={() => setShowFilters(true)}
            variant="primary"
            title="Filtrar"
          >
            <MdFilterAlt />
          </Button>
          <Link to="/add" className="btn btn-success" title="Agregar">
            <MdOutlineAddCircle />
          </Link>
        </ButtonGroup>
      </div>

      <Table responsive>
        <thead>
          <tr>
            <th>Acciones</th>
            <th>Primer Nombre</th>
            <th>Otros Nombre</th>
            <th>Primer Apellido</th>
            <th>Segundo Apellido</th>
            <th>Pais</th>
            <th>Tipo Adentificacion</th>
            <th>Identificacion</th>
            <th>Correo Electrónico</th>
            <th>Fecha Ingreso</th>
            <th>Area</th>
            <th>Estado</th>
            <th>Fecha Registro</th>
          </tr>
        </thead>
        <tbody>
          {paginate.data.map(empleado => (
            <tr key={empleado.id}>
              <td>
                <ButtonGroup>
                  <Button
                    variant="danger"
                    className="text-white"
                    title="Eliminar"
                    onClick={() => handleDelete(empleado.id)}
                  >
                    <MdOutlineDelete />
                  </Button>
                  <Link
                    to={`/${empleado.id}`}
                    className="btn btn-primary"
                    title="editar"
                  >
                    <MdOutlineModeEditOutline />
                  </Link>
                </ButtonGroup>
              </td>
              <td>{empleado.primer_nombre}</td>
              <td>{empleado.otros_nombre}</td>
              <td>{empleado.primer_apellido}</td>
              <td>{empleado.segundo_apellido}</td>
              <td>{empleado.pais_nombre}</td>
              <td>{empleado.tipo_identificacion_nombre}</td>
              <td>{empleado.identificacion}</td>
              <td>{empleado.correo_electronico}</td>
              <td>{empleado.fecha_ingreso}</td>
              <td>{empleado.area.nombre}</td>
              <td>
                <Badge bg={empleado.estado === '1' ? 'success' : 'danger'}>
                  {empleado.estado === '1' ? 'Activo' : 'Inactivo'}
                </Badge>
              </td>
              <td>{empleado.fecha_registro}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* Paginación */}
      <div className="d-flex justify-content-center pt-3">
        <ButtonToolbar>
          <ButtonGroup className="me-2" aria-label="First group">
            {paginate.links.map(link => (
              <Button
                disabled={!link.url}
                onClick={() => getData(link.url)}
                active={link.active}
                key={link.label}
              >
                {ICONS_PAGINATE[link.label] || link.label}
              </Button>
            ))}
          </ButtonGroup>
        </ButtonToolbar>
      </div>

      <FiltrosEmpleado
        showFilters={showFilters}
        onClose={handleClose}
        getData={getData}
        filtros={filtros}
        onChangeFiltros={onChangeFiltros}
      />
    </>
  );
};
