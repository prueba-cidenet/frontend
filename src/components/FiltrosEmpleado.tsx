import { useState, useEffect, ChangeEvent } from 'react';
import { Modal, Row, Col, Form, Button } from 'react-bootstrap';
import { getTiposIdentificacion } from '@/services/TiposIdentificacionService';
import { getPaises } from '@/services/PaisService';
import type { Paises } from '@/types/Paises';
import type { TiposIdentificacion } from '@/types/TiposIdentificacion';
import type { EmpleadoFiltro } from '@/types/EmpleadoFiltro';

type Props = {
  showFilters: boolean;
  getData: (url?: string) => void;
  onClose: () => void;
  filtros: EmpleadoFiltro;
  onChangeFiltros: (filtros: EmpleadoFiltro) => void;
};

export const FiltrosEmpleado = ({
  showFilters,
  getData,
  onClose,
  filtros,
  onChangeFiltros,
}: Props) => {
  const [tiposIdentificacion, setTiposIdentificacion] =
    useState<TiposIdentificacion>([]);
  const [paises, setPaises] = useState<Paises>([]);

  useEffect(() => {
    getTiposIdentificacion().then(response =>
      setTiposIdentificacion(response.data)
    );
    getPaises().then(response => setPaises(response.data));
  }, []);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) =>
    onChangeFiltros({ ...filtros, [e.target.name]: e.target.value });

  const handleChangeSelect = (e: ChangeEvent<HTMLSelectElement>) =>
    onChangeFiltros({ ...filtros, [e.target.name]: e.target.value });

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    getData();
    onClose();
  };

  return (
    <Modal show={showFilters} onHide={onClose} size="lg">
      <Form onSubmit={handleSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>Filtros Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col xs={6}>
              <Form.Group className="mb-3" controlId="primer_nombre">
                <Form.Label>Primer Nombre</Form.Label>
                <Form.Control
                  value={filtros.primer_nombre}
                  onChange={handleChange}
                  name="primer_nombre"
                  type="text"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="primer_apellido">
                <Form.Label>Primer Apellido</Form.Label>
                <Form.Control
                  value={filtros.primer_apellido}
                  onChange={handleChange}
                  name="primer_apellido"
                  type="text"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="tipo_identificacion">
                <Form.Label>Tipo de identificacion</Form.Label>
                <Form.Select
                  value={filtros.tipo_identificacion}
                  onChange={handleChangeSelect}
                  name="tipo_identificacion"
                >
                  <option>Seleccione una opción</option>
                  {tiposIdentificacion.map(tipo => (
                    <option value={tipo.id} key={tipo.id}>
                      {tipo.nombre}
                    </option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group className="mb-3" controlId="pais">
                <Form.Label>País</Form.Label>
                <Form.Select
                  value={filtros.pais}
                  onChange={handleChangeSelect}
                  name="pais"
                >
                  <option>Seleccione una opción</option>
                  {paises.map(tipo => (
                    <option value={tipo.id} key={tipo.id}>
                      {tipo.nombre}
                    </option>
                  ))}
                </Form.Select>
              </Form.Group>
              <Form.Group className="mb-3" controlId="estado">
                <Form.Label>Estado</Form.Label>
                <Form.Select
                  value={filtros.estado}
                  onChange={handleChangeSelect}
                  name="estado"
                >
                  <option>Seleccione una opción</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col xs={6}>
              <Form.Group className="mb-3" controlId="otros_nombre">
                <Form.Label>Otros Nombre</Form.Label>
                <Form.Control
                  value={filtros.otros_nombre}
                  onChange={handleChange}
                  name="otros_nombre"
                  type="text"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="segundo_apellido">
                <Form.Label>Segundo Apellido</Form.Label>
                <Form.Control
                  value={filtros.segundo_apellido}
                  onChange={handleChange}
                  name="segundo_apellido"
                  type="text"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="identificacion">
                <Form.Label>Identificacion</Form.Label>
                <Form.Control
                  value={filtros.identificacion}
                  onChange={handleChange}
                  name="identificacion"
                  type="text"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="correo_electronico">
                <Form.Label>Correo electrónico</Form.Label>
                <Form.Control
                  value={filtros.correo_electronico}
                  onChange={handleChange}
                  name="correo_electronico"
                  type="text"
                />
              </Form.Group>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClose}>
            Cerrar
          </Button>
          <Button variant="primary" type="submit">
            Buscar
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};
