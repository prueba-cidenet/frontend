import { useState } from 'react';
import { saveEmpleado } from '@/services/EmpleadoService';
import { toast } from 'react-toastify';
import type { EmpleadoForm } from '@/types/Empleado';
import { FormEmpleado } from './FormEmpleado';

const INITIAL_STATE = {
  primer_nombre: '',
  otros_nombre: '',
  primer_apellido: '',
  segundo_apellido: '',
  pais: '',
  tipo_identificacion: '',
  identificacion: '',
  fecha_ingreso: new Date(),
  area_id: 0,
  fecha_registro: new Date(),
};

export const AgregarEmpleado = () => {
  const [empleado, setEmpleado] = useState<EmpleadoForm>(INITIAL_STATE);
  const [errores, setErrores] = useState<string[]>([]);

  const onChangeForm = (empleadoForm: EmpleadoForm) =>
    setEmpleado(empleadoForm);

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    saveEmpleado(empleado)
      .then(() => {
        toast.success('Empleado creado con exito!');
        setEmpleado(INITIAL_STATE);
      })
      .catch(error => {
        if (error.code === 'ERR_BAD_REQUEST') {
          const erroresResponse = error.response.data.errors[0];
          const stringErrors: string[] = [];

          Object.keys(erroresResponse).forEach(key =>
            erroresResponse[key].forEach((item: string) => {
              stringErrors.push(item);
            })
          );

          setErrores(stringErrors);
          toast.error('Ocurrieron errores al guardar el empleado.');
        }
      });
  };

  return (
    <>
      <h1 className="h2 pb-3">Agregar Empleado</h1>

      <FormEmpleado
        onSubmit={handleSubmit}
        empleado={empleado}
        onChangeForm={onChangeForm}
        errores={errores}
        isCreate
      />
    </>
  );
};
