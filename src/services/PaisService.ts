import type { Paises } from '@/types/Paises';
import { http } from '../http';

export const getPaises = () => http.get<Paises>('/paises');
