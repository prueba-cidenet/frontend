import type { Empleado, EmpleadoForm } from '@/types/Empleado';
import type { EmpleadoFiltro } from '@/types/EmpleadoFiltro';
import type { PaginateEmpleados } from '@/types/PaginateEmpleados';
import { http } from '../http';

export const getEmpleados = (url: string, filtros: EmpleadoFiltro) =>
  http.get<PaginateEmpleados>(url, {
    params: {
      filtros: JSON.stringify(filtros),
    },
  });

export const saveEmpleado = (empleado: EmpleadoForm) =>
  http.post<Empleado>('/empleados', empleado);

export const getEmpleado = (id: number) =>
  http.get<Empleado>(`/empleados/${id}`);

export const updateEmpleado = (id: number, empleado: EmpleadoForm) =>
  http.put<Empleado>(`/empleados/${id}`, empleado);

export const deleteEmpleado = (id: number) =>
  http.delete<Empleado>(`/empleados/${id}`);
