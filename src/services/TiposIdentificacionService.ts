import type { TiposIdentificacion } from '@/types/TiposIdentificacion';
import { http } from '../http';

export const getTiposIdentificacion = () =>
  http.get<TiposIdentificacion>('/tiposIdentificacion');
