import type { Areas } from '@/types/Areas';
import { http } from '../http';

export const getAreas = () => http.get<Areas>('/areas');
