# Fronted Prueba Cidenet

Proyecto desarrollado en react que interactúa con el backend para el manejo de empleados

## Configuración

Ejecutar los siguientes comandos:

```
npm install && npm start
```

El proyecto se ejecutará por defecto en [localhost:3000](http://localhost:3000)